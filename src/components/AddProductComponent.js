import React from 'react';
import { Button } from 'react-bootstrap';

function AddProductComponent( props ) {

    return(
        <form>
            <h3>{ props.state.edit ? 'Edit' : 'Add' } Product Form</h3>
            <table id = 'productform'>
                <tbody>
                    <tr>
                        <td>
                            <p className = 'label' >Product Name :</p>
                            <input
                                name = 'productName'
                                type = 'text'
                                value = { props.state.productName }
                                onChange = { ( e ) => { props.change( e ) } }
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label' >Category :</p>
                            <input
                                name = 'category'
                                type = 'text'
                                value = { props.state.category }
                                onChange = { ( e ) => { props.change( e ) } } 
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label' >Price :</p>
                            <input
                                name = 'price'
                                type = 'text'
                                value = { props.state.price }
                                onChange = { ( e ) => { props.change( e ) } }
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Button
                                id = 'submitbutton'
                                variant = 'primary'
                                onClick = { () => props.submit( props.state.id ) } >
                                { props.state.edit ? 'Edit Product' : 'Add Product' }
                            </Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    );
}

export default AddProductComponent;