import React from 'react';
import { Button } from 'react-bootstrap';

function LoginUserComponent( props ) {
    return(
        <form>
            <h3> Login User </h3>
            <table id = 'productform'>
                <tbody>
                    <tr>
                        <td>
                            <p className = 'label' >Email :</p>
                            <input
                                name = 'email'
                                type = 'text'
                                value = { props.state.email }
                                onChange = { ( e ) => { props.change( e ) } }
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label' >Password :</p>
                            <input
                                name = 'password'
                                type = 'text'
                                value = { props.state.password }
                                onChange = { ( e ) => { props.change( e ) } } 
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Button
                                id = 'submitbutton'
                                variant = 'primary'
                                onClick = { () => props.submit() } >
                                Login
                            </Button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href = '/register'>Register as a User</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    );
}

export default LoginUserComponent;