import React from 'react';
import { Container, Table, Button } from 'react-bootstrap';

function HomeComponent ( props ) {
    return (
        <>
            <h1> Works Correctly </h1>
            <Container>
                <Table id = 'productsTable'>
                    <thead>
                        <tr>
                            <th> # </th>
                            <th> Name </th>
                            <th> Category </th>
                            <th> Price </th>
                            <th> options </th>
                        </tr>
                    </thead>
                    <tbody>
                        { props.products.map( ( product, index ) => 
                            <tr key = { product.id } >
                                <td> { index } </td>
                                <td> { product.name } </td>
                                <td> { product.category } </td>
                                <td> Rs.{ product.price }/- </td>
                                <td>
                                    <Button 
                                        variant = 'primary'
                                        className = 'options'
                                        onClick = { () => { props.edit( product.id ) } } >
                                        Edit
                                    </Button>
                                    <Button
                                        variant = 'primary'
                                        className = 'options'
                                        onClick = { () => { props.delete( product.id ) } } >
                                        Delete
                                    </Button>
                                </td>
                            </tr>
                        ) }
                    </tbody>
                </Table>
                <Button variant = 'warning' onClick = { () => { props.edit() } } >
                    Add Product
                </Button>
            </Container>
        </>
    );
}

export default HomeComponent;