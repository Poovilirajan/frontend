import React from 'react';
import { Button } from 'react-bootstrap';

function CreateUserComponent( props ) {
    return(
        <form>
            <h3> User Registration </h3>
            <table id = 'productform'>
                <tbody>
                    <tr>
                        <td>
                            <p className = 'label' >Firstname :</p>
                            <input
                                name = 'firstName'
                                type = 'text'
                                value = { props.state.firstName }
                                onChange = { ( e ) => { props.change( e ) } }
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label' >Lastname :</p>
                            <input
                                name = 'lastName'
                                type = 'text'
                                value = { props.state.lastName }
                                onChange = { ( e ) => { props.change( e ) } }
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label' >Email :</p>
                            <input
                                name = 'email'
                                type = 'text'
                                value = { props.state.email }
                                onChange = { ( e ) => { props.change( e ) } }
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label' >Password :</p>
                            <input
                                name = 'password'
                                type = 'text'
                                value = { props.state.password }
                                onChange = { ( e ) => { props.change( e ) } } 
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Button
                                id = 'submitbutton'
                                variant = 'primary'
                                onClick = { () => props.submit() } >
                                Register
                            </Button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href = '/'>Already have an Account</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    );
}

export default CreateUserComponent;