import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LoginUser from '../containers/LoginUser';
import CreateUser from '../containers/CreateUser';
import MyProfile from '../containers/MyProfile';

function MainRouter ( props ) {

    return (
        <BrowserRouter>
            <Switch>
                <Route exact path = '/' component = { LoginUser } />
                <Route path = '/register' component = { CreateUser } />
                <Route path = '/profile' component = { MyProfile } />
            </Switch>
        </BrowserRouter>
    );
}

export default MainRouter;