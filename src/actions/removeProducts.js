import axios from 'axios';

export function removeProduct( id ) {
    return axios.delete( `/products/${ id }` );
}