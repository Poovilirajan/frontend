import axios from 'axios';

export function getProducts() {
    return axios.get( '/products/' );
}

export function findProduct( id ) {
    return axios.get( `/products/${ id }` );
}