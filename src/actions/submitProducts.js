import axios from 'axios';

export function addProduct( data ) {
    return axios( {
        method: 'post',
        url: '/products/',
        data: data
    } );
}

export function editProduct( id, data ) {
    return axios( {
        method: 'put',
        url: `/products/${ id }`,
        data: data
    } );
}