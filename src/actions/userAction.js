import axios from 'axios';

export function loginAction( data ) {
    return axios( {
        method: 'post',
        url: '/user/login',
        data: data
    } );

}

export function registerAction( data ) {
    return axios( {
        method: 'post',
        url: '/user',
        data: data
    } );

}

export function getUserDetails( token ){
    return axios( {
        method: 'get',
        url: `/user/${ token }`
    } );
}