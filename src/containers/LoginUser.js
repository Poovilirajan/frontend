import React from 'react';
import { loginAction } from '../actions/userAction';
import LoginUserComponent from '../components/LoginUserComponent';

class LoginUser extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            email: '',
            password: ''
        }
    }

    handleChange = ( { target } = {} ) => {
        if ( target ) {
            let { name, value } = target;
            this.setState( { [ name ]: value } );
        }
    }

    handleSubmit = () => {
        loginAction( this.state )
        .then ( ( res ) => {
            alert( res.data.message );
            window.localStorage.setItem( 'authToken', res.data.token );
            this.props.history.push( '/profile' );
        } )
        .catch ( ( err ) => {
            alert( 'Invalid email or password' )
            console.log( err );
        } );
    }
    
    render() {
        return ( 
            <LoginUserComponent 
                change = { this.handleChange }
                state = { this.state }
                submit = { this.handleSubmit }
            />
        );
    }
}

export default LoginUser;