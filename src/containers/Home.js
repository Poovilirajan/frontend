import React from 'react';
import { getProducts } from '../actions/getProducts';
import { removeProduct } from '../actions/removeProducts';
import HomeComponent from '../components/HomeComponent';

class Home extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            products: []
        };
    }

    componentDidMount() {
        this.getProductsFromApi();
    }

    getProductsFromApi() {
        getProducts()
        .then( ( res ) => {
            this.setState( { products: res.data } );
        } )
        .catch( ( err ) => {
            console.log( err );
        } );
    }

    handleEdit = ( id ) => {
        if ( id ) {
            this.props.history.push( `/add/${ id }` );
        } else {
            this.props.history.push( '/add/0' );
        }
    }

    handleDelete = ( id ) => {
        removeProduct( id )
        .then( ( res ) => {
            alert( res.data );
            this.getProductsFromApi();
        } )
        .catch( ( err ) => {
            alert( err.message );
        } );
    }

    render() {
        return ( 
            <HomeComponent
                products = { this.state.products }
                edit = { this.handleEdit }
                delete = { this.handleDelete }
            />
        );
    }
}

export default Home;