import React from 'react';
import { getUserDetails } from '../actions/userAction';

class MyProfile extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            firstname: '',
            lastname: '',
            email: ''
        };
    }

    componentDidMount(){
        getUserDetails( window.localStorage.getItem( 'authToken' ) )
        .then( ( res ) => {
            console.log( res );
            this.setState( {
                firstname: res.data.firstname,
                lastname: res.data.lastname,
                email: res.data.email
            } );
        } )
        .catch( ( err ) => {
            console.log( err );
        } )
    }

    render() {
        return (
            <form>
                <h3> My Details </h3>
                <table id = 'productform'>
                    <tbody>
                        <tr>
                            <td>
                                <p className = 'label' > Firstname </p>
                            </td>
                            <td>
                                <p className = 'label' > : { this.state.firstname } </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label' > Lastname </p>
                            </td>
                            <td>
                                <p className = 'label' >: { this.state.lastname } </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label' > Email </p>
                            </td>
                            <td>
                                <p className = 'label' > : { this.state.email } </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        );
    }
}

export default MyProfile;