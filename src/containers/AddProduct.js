import React from 'react';
import AddProductComponent from '../components/AddProductComponent';
import { findProduct } from '../actions/getProducts';
import { addProduct, editProduct } from '../actions/submitProducts';

class AddProduct extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            id: undefined,
            productName: '',
            category: '',
            price: '',
            edit: false
        };
    }

    componentDidMount() {
        if ( this.props.match.params.id !== '0' ) {
            findProduct( this.props.match.params.id )
            .then( ( res ) => {
                this.setState( {
                    id: res.data.id,
                    productName: res.data.name,
                    category: res.data.category,
                    price: res.data.price,
                    edit: true
                } );
            } )
            .catch ( ( err ) => {
                console.log( err );
            } );
        }
        else {
            console.log('not fine');
        }
    }

    handleChange = ( { target } = {} ) => {
        if ( target ) {
            let { name, value } = target;
            this.setState( { [ name ]: value } );
        }
    }

    handleSubmit = ( id ) => {
        console.log( this.state );
        const data = {
            name: this.state.productName,
            category: this.state.category,
            price: this.state.price
        };
        if( id ){
            editProduct( id, data )
            .then( ( res ) => {
                alert( res.data );
                this.props.history.push( '/' );
            } )
            .catch( ( err ) => {
                alert( err.message );
            } );
        } else {
            addProduct( data )
            .then( ( res ) => {
                alert( res.data );
                this.props.history.push( '/' );
            } )
            .catch( ( err ) => {
                alert( err.message );
            } );
        }
    }

    render() {
        return ( 
            <AddProductComponent
                change = { this.handleChange }
                state = { this.state }
                submit = { this.handleSubmit }
            /> );
    }
}

export default AddProduct;