import React from 'react';
import CreateUserComponent from '../components/CreateUserComponent';
import { registerAction } from '../actions/userAction';

class CreateUser extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: ''
        };
    }

    handleChange = ( { target } = {} ) => {
        if ( target ) {
            let { name, value } = target;
            this.setState( { [ name ]: value } );
        }
    }

    handleSubmit = () => {
        registerAction( this.state )
        .then( ( res ) => {
            alert( res.data.message );
            this.props.history.push( '/' );
        } )
        .catch( ( err ) => {
            alert( 'Invalid Request ! ');
            console.log( err );
        } );
    }

    render() {
        return (
            <CreateUserComponent
                change = { this.handleChange }
                state = { this.state }
                submit = { this.handleSubmit }
            />
        );
    }
}

export default CreateUser;